import Base from '@baseEntity';
import { LoginTypes } from '@enums/loginType';
import { Roles } from '@enums/role';
import { UserStatues } from '@enums/userStatus';
import { BeforeCreate, Entity, Property } from '@mikro-orm/core';
import UserValidator from '@validators/user.validator';
import { hashSync } from 'bcrypt';
import { Field, ObjectType } from 'type-graphql';

@Entity({ tableName: 'users' })
@ObjectType({ description: 'The User model' })
export default class User extends Base<User> {
     @Field(() => String, { nullable: true })
     @Property({ nullable: true })
     email: String;

     @Field((_type) => String, { nullable: true })
     @Property({ nullable: true })
     facebookId: String;

     @Field((_type) => String, { nullable: true })
     @Property({ nullable: true })
     instagramId: String;

     @Field((_type) => String, { nullable: true })
     @Property({ nullable: true })
     livingRoomName: String;

     @Field((_type) => String, { nullable: true })
     @Property({ nullable: true })
     name: String;

     @Field((_type) => String, { nullable: true })
     @Property({ nullable: true })
     surName: String;

     @Field((_type) => String, { nullable: true })
     @Property({ nullable: true })
     address: String;

     @Field((_type) => String, { nullable: true })
     @Property({ nullable: true })
     clubName: String;

     @Field((_type) => String, { nullable: true })
     @Property({ nullable: true })
     description: String;

     @Field((_type) => String, { nullable: true })
     @Property({ nullable: true })
     appLanguage: String;

     @Field((_type) => String, { nullable: true })
     @Property({ nullable: true })
     fbLink: String;

     @Field((_type) => String, { nullable: true })
     @Property({ nullable: true })
     twitterLink: String;

     @Field((_type) => String, { nullable: true })
     @Property({ nullable: true })
     linkedInLink: String;

     @Field((_type) => String, { nullable: true })
     @Property({ nullable: true })
     instagramLink: String;

     @Field((_type) => String, { nullable: true })
     @Property({ nullable: true })
     timezone: String;

     @Field((_type) => String, { nullable: true })
     @Property({ nullable: true })
     companyName: String;

     @Field((_type) => [String], { nullable: true })
     @Property({ nullable: true })
     languages: [String];

     //integers
     @Field((_type) => Number, { nullable: true })
     @Property({ nullable: true })
     eventsOrganised: Number;

     @Field((_type) => Number, { nullable: true })
     @Property({ nullable: true })
     overallRating: Number;

     @Field((_type) => Number, { nullable: true })
     @Property({ nullable: true })
     age: Number;

     //boolean
     @Field((_type) => Boolean, { nullable: true })
     @Property({ default: false })
     isEmailVerified: Boolean;

     @Field((_type) => Boolean, { nullable: true })
     @Property({ default: false })
     isLive: Boolean;

     @Field((_type) => Boolean, { nullable: true })
     @Property({ default: true })
     isNotifications: Boolean;

     // enums
     @Field((_type) => LoginTypes, { nullable: true })
     @Property({ nullable: true })
     loginType: LoginTypes;

     @Field((_type) => Roles, { nullable: true })
     @Property({ default: Roles.USER })
     role: Roles;

     @Field((_type) => UserStatues, { nullable: true })
     @Property({ default: UserStatues.PENDING })
     status: String;

     @Property({ nullable: true })
     activationCode: String;

     @Property({ nullable: true })
     password: String;

     @BeforeCreate()
     setPassword() {
          if (this.password) {
               this.password = hashSync(this.password as string, 10);
          }
     }

     constructor(body: UserValidator) {
          super(body);
     }
}
