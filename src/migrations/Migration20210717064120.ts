import { Migration } from '@mikro-orm/migrations';

export class Migration20210717064120 extends Migration {

  async up(): Promise<void> {
    this.addSql('create table `users` (`id` varchar(255) not null, `createdAt` datetime not null, `updatedAt` datetime not null, `email` varchar(255) null, `facebookId` varchar(255) null, `instagramId` varchar(255) null, `livingRoomName` varchar(255) null, `name` varchar(255) null, `surName` varchar(255) null, `address` varchar(255) null, `clubName` varchar(255) null, `description` varchar(255) null, `appLanguage` varchar(255) null, `fbLink` varchar(255) null, `twitterLink` varchar(255) null, `linkedInLink` varchar(255) null, `instagramLink` varchar(255) null, `timezone` varchar(255) null, `companyName` varchar(255) null, `languages` text null, `eventsOrganised` int(11) null, `overallRating` int(11) null, `age` int(11) null, `isEmailVerified` tinyint(1) not null default false, `isLive` tinyint(1) not null default false, `isNotifications` tinyint(1) not null default true, `loginType` varchar(255) null, `role` varchar(255) not null default \'user\', `status` varchar(255) not null default \'pending\', `activationCode` varchar(255) null, `password` varchar(255) null) default character set utf8mb4 engine = InnoDB;');
    this.addSql('alter table `users` add primary key `users_pkey`(`id`);');
  }

}
