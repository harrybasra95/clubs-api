import { Migration } from '@mikro-orm/migrations';

export class Migration20210717063921 extends Migration {

  async up(): Promise<void> {
    this.addSql('create table `users` (`id` varchar(255) not null, `createdAt` datetime not null, `updatedAt` datetime not null, `email` varchar(255) not null, `facebookId` varchar(255) not null, `instagramId` varchar(255) not null, `livingRoomName` varchar(255) not null, `name` varchar(255) not null, `surName` varchar(255) not null, `address` varchar(255) not null, `clubName` varchar(255) not null, `description` varchar(255) not null, `appLanguage` varchar(255) not null, `fbLink` varchar(255) not null, `twitterLink` varchar(255) not null, `linkedInLink` varchar(255) not null, `instagramLink` varchar(255) not null, `timezone` varchar(255) not null, `companyName` varchar(255) not null, `languages` text not null, `eventsOrganised` int(11) not null, `overallRating` int(11) not null, `age` int(11) not null, `isEmailVerified` tinyint(1) not null default false, `isLive` tinyint(1) not null default false, `isNotifications` tinyint(1) not null default true, `loginType` varchar(255) not null, `role` varchar(255) not null default \'user\', `status` varchar(255) not null default \'pending\', `activationCode` varchar(255) not null, `password` varchar(255) not null) default character set utf8mb4 engine = InnoDB;');
    this.addSql('alter table `users` add primary key `users_pkey`(`id`);');
  }

}
