import { registerEnumType } from 'type-graphql';

export enum LoginTypes {
     EMAIL = 'email',
     FACEBOOK = 'facebook',
     INSTAGRAM = 'instagram',
}

registerEnumType(LoginTypes, {
     name: 'LoginTypes',
     description: 'User login types',
});
