import { LoginTypes } from '@enums/loginType';
import { Roles } from '@enums/role';
import { IsEmail, IsString } from 'class-validator';
import { Field, InputType } from 'type-graphql';

@InputType()
export default class UserValidator {
     @Field({ nullable: true })
     @IsEmail()
     email: String;

     @Field({ nullable: true })
     @IsString()
     facebookId: String;

     @Field({ nullable: true })
     @IsString()
     instagramId: String;

     @Field({ nullable: true })
     @IsString()
     livingRoomName: String;

     @Field({ nullable: true })
     @IsString()
     name: String;

     @Field({ nullable: true })
     @IsString()
     surName: String;

     @Field({ nullable: true })
     @IsString()
     address: String;

     @Field({ nullable: true })
     @IsString()
     clubName: String;

     @Field({ nullable: true })
     @IsString()
     description: String;

     @Field({ nullable: true })
     @IsString()
     appLanguage: String;

     @Field({ nullable: true })
     @IsString()
     fbLink: String;

     @Field({ nullable: true })
     @IsString()
     twitterLink: String;

     @Field({ nullable: true })
     @IsString()
     linkedInLink: String;

     @Field({ nullable: true })
     @IsString()
     instagramLink: String;

     @Field({ nullable: true })
     @IsString()
     timezone: String;

     @Field({ nullable: true })
     @IsString()
     companyName: String;

     @Field((_type) => [String], { nullable: true })
     languages: [String];

     //integers
     @Field({ nullable: true })
     eventsOrganised: Number;

     @Field({ nullable: true })
     overallRating: Number;

     @Field({ nullable: true })
     age: Number;

     @Field({ nullable: true })
     isLive: Boolean;

     @Field({ nullable: true })
     isNotifications: Boolean;

     // enums
     @Field((_type) => LoginTypes)
     @IsString()
     loginType: LoginTypes;

     @Field((_type) => Roles)
     @IsString()
     role: Roles;

     @Field({ nullable: true })
     @IsString()
     status: String;

     @Field({ nullable: true })
     @IsString()
     password: String;
}
